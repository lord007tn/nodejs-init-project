/* eslint-disable no-undef */
const express = require( "express" );
const app = express();
const dotenv = require( "dotenv" );
const mongoose = require( "mongoose" );


dotenv.config();
//Connect to database
mongoose.set( "useNewUrlParser", true );
mongoose.set( "useFindAndModify", false );
mongoose.set( "useCreateIndex", true );
mongoose.set( "useUnifiedTopology", true );
mongoose.connect( process.env.DB_CONNECT, () => console.log( "connected to db" ) );

//Middleware
app.use( express.json() );
app.use( express.urlencoded() );
//Route Middleware


//Server Run
let port = 8000;
app.listen( port, () => {
	console.log( "Server is up and running on port number " + port );
} );